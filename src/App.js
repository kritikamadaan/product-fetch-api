import React from 'react';

import './App.css';
import ProductLists from './ProductLists';

function App() {
  return (
    <div className="App">
      <ProductLists/>
    </div>
  );
}

export default App;
